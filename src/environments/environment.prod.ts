import env from './.env';

export const environment = {
    production: true,
    ci: 1,
    cs: 'cbc6cb25-d7bc-42dc-b10b-3cf501b2d916',
    gt: 'password',
    version: env.npm_package_version + '-dev',
    serverUrl: '/api',
    defaultLanguage: 'en-US',
    supportedLanguages: [
        'en-US',
        'fr-FR'
    ]
};
