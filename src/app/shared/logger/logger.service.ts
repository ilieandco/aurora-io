import {Injectable } from '@angular/core';
import {LoggerServiceInterface} from '@app/shared/logger/logger.service.interface';

/**
 * The possible log levels.
 * LogLevel.Off is never emitted and only used with Logger.level property to disable logs.
 */
export enum LogLevel {
    Off = 0,
    Error,
    Warning,
    Info,
    Debug
}

/**
 * Log output handler function.
 */
export type LogOutput = (source: string, level: LogLevel, ...objects: any[]) => void;

export class LoggerService implements LoggerServiceInterface {
    /**
     * Current logging level.
     * Set it to LogLevel.Off to disable logs completely.
     */
    static level = LogLevel.Debug;

    /**
     * Additional log outputs.
     */
    static outputs: LogOutput[] = [];

    /**
     * Enables production mode.
     * Sets logging level to LogLevel.Warning.
     */
    static enableProductionMode() {
        LoggerService.level = LogLevel.Warning;
    }

    public source:string = '';

    constructor() {
        this.source = 'logger-service';
    }

    /**
     * Logs messages or objects  with the debug level.
     * Works the same as console.log().
     */
    debug(...objects: any[]) {
        this.log(console.log, LogLevel.Debug, objects);
    }


    /**
     * Logs messages or objects  with the info level.
     * Works the same as console.log().
     */
    info(...objects: any[]) {
        this.log(console.info, LogLevel.Info, objects);
    }

    /**
     * Logs messages or objects  with the warning level.
     * Works the same as console.log().
     */
    warn(...objects: any[]) {
        this.log(console.warn, LogLevel.Warning, objects);
    }

    /**
     * Logs messages or objects  with the error level.
     * Works the same as console.log().
     */
    error(...objects: any[]) {
        this.log(console.error, LogLevel.Error, objects);
    }

    private log(func: Function, level: LogLevel, objects: any[]) {
        if (level <= LoggerService.level) {
            const log = this.source ? ['[' + this.source + ']'].concat(objects) : objects;
            func.apply(console, log);
            LoggerService.outputs.forEach((output) => output.apply(output, [this.source, level].concat(objects)));
        }
    }
}
