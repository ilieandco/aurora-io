export interface LoggerServiceInterface {
    debug(...objects: any[]): void;
    info(...objects: any[]): void;
    warn(...objects: any[]): void;
    error(...objects: any[]): void;
}
