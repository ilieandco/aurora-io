import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {finalize} from 'rxjs/operators';
import {AuthenticationService} from '@app/core/auth/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent
    implements OnInit {

    loginForm: FormGroup;
    loginError: string;

    constructor(
        protected _fb: FormBuilder,
        protected authService: AuthenticationService,
        private formBuilder: FormBuilder
    ) {
        this.createForm();
    }

    ngOnInit(): void {

    }

    create() {
        this.authService.login(this.loginForm.value)
            .pipe(finalize(() => {
                console.log('finalize inside pipe 1');
            }))
            .subscribe(credentials => {
                console.debug('login.component.credentials', credentials);

                console.log(`${credentials.username} successfully logged in`);
                // this.router.navigate(['/'], {replaceUrl: true});
            }, error => {
                console.debug(`Login error: ${error}`);
                // this.error = error;
            });
    }

    private createForm() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            remember: true
        });
    }
}
