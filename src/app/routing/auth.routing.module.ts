import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LogoutComponent} from '@app/components/logout/logout.component';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';

    const routes: Routes = [
        {
            path: 'logout',
            component: LogoutComponent,
        },
];

    @NgModule({
            imports: [
            CommonModule,
            BrowserModule,
            RouterModule.forRoot(routes),
            ReactiveFormsModule
        ],
        exports: [ RouterModule ],
        declarations: [LogoutComponent]
})
export class AuthRoutingModule {
}
