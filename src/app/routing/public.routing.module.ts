import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from '@app/components/login/login.component';
import {SignupComponent} from "@app/components/signup/signup.component";

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'signup',
        component: SignupComponent,
    },
    {redirectTo: 'login', path: '**', pathMatch: 'full'}

];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes),
        ReactiveFormsModule
    ],
    declarations: [LoginComponent, SignupComponent],
    exports: [RouterModule]
})
export class PublicRoutingModule {
}
