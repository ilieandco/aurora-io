import {Component, Inject, OnInit} from '@angular/core';
import {LoggerService} from '@app/shared/logger/logger.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title:string;

    constructor(
        // protected log: LoggerService
        // protected log: LoggerService
    ) {
        this.title = 'aurora-web-v2';
        // this.log.debug('app component log', log);
    }

    ngOnInit(): void {
        // console.log('constructed', this.log);
    }


}
