import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AuthenticationService} from '@app/core/auth/auth.service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {HttpCacheService} from '@app/core/http/http-cache.service';
import {ApiPrefixInterceptor} from '@app/core/http/api-prefix.interceptor';
import {ErrorHandlerInterceptor} from '@app/core/http/error-handler.interceptor';
import {CacheInterceptor} from '@app/core/http/cache.interceptor';
import {HttpService} from '@app/core/http/http.service';
import {PublicRoutingModule} from '@app/routing/public.routing.module';
import {AuthRoutingModule} from '@app/routing/auth.routing.module';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        PublicRoutingModule,
        AuthRoutingModule,
        HttpClientModule
    ],
    // providers: [LoggerService],
    providers: [
        AuthenticationService,
        HttpCacheService,
        ApiPrefixInterceptor,
        ErrorHandlerInterceptor,
        CacheInterceptor,
        { provide: HttpClient, useClass: HttpService },
        // TODO:: check object
        // { provide: 'IAuthService', useClass: AuthenticationService},
        // { provide: HTTP_INTERCEPTORS, useClass: ApiPrefixInterceptor, multi: true },
        // {provide: 'LoggerServiceInterface', useClass: LoggerService},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
