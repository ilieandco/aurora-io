import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {environment} from '@env/environment';
import {catchError, finalize, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

export interface AuthCredentials {
    // Customize received credentials here
    token_type: string;
    expires_in: number;
    access_token: string;
    refresh_token: string;
    username: string;
}

export interface AuthLoginContext {
    username: string;
    password: string;
    remember?: boolean;
}

const credentialsKey = 'credentials';

@Injectable()
export class AuthenticationService {

    private _credentials: AuthCredentials | null;

    constructor(
        private httpClient: HttpClient
    ) {
        const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
        if (savedCredentials) {
            this._credentials = JSON.parse(savedCredentials);
        }
    }

    /**
     * Authenticates the user.
     * @param {LoginContext} context The login parameters.
     * @return {Observable<Credentials>} The user credentials.
     */
    login(context: AuthLoginContext): Observable<AuthCredentials> {
        // Replace by proper authentication call
        console.log('auth.service.lgin.ts');

        const params = {
            ci: environment.ci,
            cs: environment.cs,
            gt: environment.gt,
            username: context.username,
            password: context.password
        };

        return new Observable<AuthCredentials>((observer) => {
            this.httpClient
                .post('authenticate', params)
                .pipe(
                    catchError(() => of('Error, could not authenticate')),
                )
                .subscribe((response: AuthCredentials) => {
                    console.log('auth.service.subscribed');

                    const creds = {
                        token_type: response.token_type,
                        access_token: response.access_token,
                        refresh_token: response.refresh_token,
                        expires_in: response.expires_in,
                        username: context.username,
                    };

                    observer.next(creds);
                    this.setCredentials(creds, context.remember);

                    observer.complete();
                });
        });
    }

    /**
     * Logs out the user and clear credentials.
     * @return {Observable<boolean>} True if the user was logged out successfully.
     */
    logout(): Observable<boolean> {
        // Customize credentials invalidation here
        this.setCredentials();
        return of(true);
    }

    /**
     * Checks is the user is authenticated.
     * @return {boolean} True if the user is authenticated.
     */
    isAuthenticated(): boolean {
        return !!this.credentials;
    }

    /**
     * Gets the user credentials.
     * @return {Credentials} The user credentials or null if the user is not authenticated.
     */
    get credentials(): AuthCredentials | null {
        return this._credentials;
    }

    /**
     * Sets the user credentials.
     * The credentials may be persisted across sessions by setting the `remember` parameter to true.
     * Otherwise, the credentials are only persisted for the current session.
     * @param {Credentials=} credentials The user credentials.
     * @param {boolean=} remember True to remember credentials across sessions.
     */
    private setCredentials(credentials?: AuthCredentials, remember?: boolean) {
        this._credentials = credentials || null;

        if (credentials) {
            const storage = remember ? localStorage : sessionStorage;
            storage.setItem(credentialsKey, JSON.stringify(credentials));
        } else {
            sessionStorage.removeItem(credentialsKey);
            localStorage.removeItem(credentialsKey);
        }
    }

}
